CREATE TABLE TBUSER(
	id SERIAL NOT NULL PRIMARY KEY,
	user_name VARCHAR(40) NOT NULL UNIQUE,
	first_name VARCHAR(40) NOT NULL,
	last_name VARCHAR(40),
	password VARCHAR(40) NOT NULL,
	is_admin BOOLEAN NOT NULL
);

CREATE TABLE TBCALCULATION(
	id SERIAL NOT NULL PRIMARY KEY,
	expression TEXT NOT NULL,
	result DOUBLE PRECISION,
	status VARCHAR(10) NOT NULL,
	user_id INTEGER REFERENCES tbuser(id) NOT NULL,
	log_timestamp TIMESTAMP NOT NULL
);