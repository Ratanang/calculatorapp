package za.co.calculator.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import calc.ejb.calculator.CalculationRequest;
import calc.ejb.calculator.CalculatorLocal;
import calc.ejb.user.AuthRequest;
import calc.ejb.user.AuthResponse;
import calc.ejb.user.CalculationResponse;
import calc.ejb.user.UserLocal;
import calc.ejb.user.UserRequest;


@ManagedBean(name="user")
@SessionScoped
public class UserController implements Serializable {
	@EJB
	UserLocal userRemote;
	@EJB
	CalculatorLocal calculator;
	
	Properties props;
	InitialContext ctx;
	private static final long serialVersionUID = 1L;
	private AuthResponse authResponse;
	private String userName;
	private String password;

	private String expression;
	private String leftOperand;
	private String rightOperand;
	private String operator;
	private String fromDate;
	private String toDate;
	private String searchUserName;
	private List<CalculationResponse> searchedExpressions;
	
	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public String getLeftOperand() {
		return leftOperand;
	}

	public void setLeftOperand(String leftOperand) {
		this.leftOperand = leftOperand;
	}

	public String getRightOperand() {
		return rightOperand;
	}

	public void setRightOperand(String rightOperand) {
		this.rightOperand = rightOperand;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public AuthResponse getAuthResponse() {
		if(authResponse == null) {
			authResponse = new AuthResponse();
		}
		return authResponse;
	}

	public void setAuthResponse(AuthResponse authResponse) {
		this.authResponse = authResponse;
	}
	
	public UserController() {
	}
	
	public String authenticate() {
//			UserLocal userRemote = (UserLocal)ctx.lookup("Calculator/UserBean!calc.ejb.user.UserLocal");
			
			AuthRequest authRequest = new AuthRequest();
			authRequest.setPassword(password);
			authRequest.setUserName(userName);
			authResponse = userRemote.authenticate(authRequest );
			
			if(authResponse.isAuthenticated()) {
				if(authResponse.getUser().isAdmin()) {
					return "admin";
				} else {
					return "basic";
				}
			} else {
				printmessage("User Name or password is incorrect!");
			}
		
		return "index";
	}
	
	public void calculate() {
		boolean isAdvance = false;

		CalculationRequest calculationRequest = new CalculationRequest();

		if (expression == null || expression.isEmpty()) {
			expression = leftOperand.concat(operator).concat(rightOperand);
			leftOperand = "";
			operator = "";
			rightOperand = "";
		} else {
			isAdvance = true;
		}

		calculationRequest.setExpression(expression);
		calculationRequest.setUserName(authResponse.getUser().getUserName());

		double outcome = calculator.calculate(calculationRequest, isAdvance);
		printmessage(String.valueOf(outcome));

		expression = "";
	}
	
	public List<CalculationResponse> findAll() {

		if (authResponse.isAuthenticated()) {
			UserRequest request = new UserRequest();
			request.setUserId(authResponse.getUser().getId());
			if (authResponse.getUser().isAdmin()) {
				return userRemote.getExpressionsForAdmin(request);
			} else {
				return userRemote.getExpressions(request);
			}
		} else {
			printmessage("You must first login!");
		}

		return new ArrayList<CalculationResponse>();
	}
	
	private void printmessage(String message) {
        FacesMessage fm = new FacesMessage(message);
        FacesContext.getCurrentInstance().addMessage("msg", fm);
    }
	

	public String search() {
		Date from = null;
		Date to = null;
		if(searchUserName != null) {
			if(fromDate != null && toDate != null) {
				
			} else if(fromDate != null) {
				
			} else if(toDate != null) {
				
			}
		} else {
			if(fromDate != null && toDate != null) {
				
			} else if(fromDate != null) {
				
			} else if(toDate != null) {
				
			}
		}
		
		if(fromDate != null && !fromDate.isEmpty()) {
			from = getDate(fromDate);
			
			if(from == null) {
				printmessage("Invalid Date/Fomart: " + fromDate);
			}
		}
		
		if(toDate != null && !toDate.isEmpty()) {
			to= getDate(toDate);
			
			if(from == null) {
				printmessage("Invalid Date/Fomart: " + toDate);
			}
		}
		
		searchedExpressions = userRemote.findByUserNameAndDateRange(searchUserName, from, to);
		
		return "results";
	}

    public Date getDate(String value)
    {
        Date date = null;
        try
        {
            DateFormat formatter;
            formatter = new SimpleDateFormat("dd/MM/yyyy");
            date = (Date) formatter.parse(value);
        } catch (ParseException e) 
        {
        	
        }

        return date;
    }

	public List<CalculationResponse> getSearchedExpressions() {
		return searchedExpressions;
	}

	public void setSearchedExpressions(List<CalculationResponse> searchedExpressions) {
		this.searchedExpressions = searchedExpressions;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getSearchUserName() {
		return searchUserName;
	}

	public void setSearchUserName(String searchUserName) {
		this.searchUserName = searchUserName;
	}
	
}
