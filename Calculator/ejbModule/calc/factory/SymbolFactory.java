package calc.factory;

import java.io.StreamTokenizer;

import calc.constant.Constants;
import calc.constant.SymbolType;
import calc.token.Addition;
import calc.token.Division;
import calc.token.Multiplication;
import calc.token.Operand;
import calc.token.Power;
import calc.token.Root;
import calc.token.Subtraction;
import calc.token.Symbol;
import calc.utils.SymbolUtils;

public class SymbolFactory {
	public static final Symbol getInstance(String token) {	
		if(SymbolUtils.isOperator(token)) {
			return newOperator(token);
		} else if(SymbolUtils.isFunction(token)) {
			return newFunction(token);
		} else if(SymbolUtils.isConstant(token)) {
			return newConstant(token);
		} else if(SymbolUtils.isNumber(token)) {
			return new Operand(token, SymbolUtils.getNumber(token));
		} else {
			return new Symbol(token);
		}
	}
	
	public static final Symbol newOperator(String token) {	
		switch(token) {
		case Constants.PLUS:
			return new Addition();
		case Constants.MINUS:
			return new Subtraction();
		case Constants.DIVIDE:
			return new Division();
		case Constants.TIMES:
			return new Multiplication();
		case Constants.POWER:
			return new Power();
		default:
			return new Symbol(SymbolType.UNKNOWN,token);
		}
	}
	
	public static final Symbol newConstant(String token) {	
		switch(token) {
		case Constants.PI:
			return new Operand(token);
		case Constants.E:
			return new Operand(token);
		default:
			return new Symbol(SymbolType.UNKNOWN,token);
		}
	}
	
	public static final Symbol newFunction(String token) {	
		switch(token) {
		case Constants.ROOT:
			return new Root();
		default:
			return new Symbol(SymbolType.UNKNOWN, token);
		}
	}
	
	public static final Symbol getInstance(StreamTokenizer tokenizer) {	
		if(tokenizer == null) {
			return null;
		}
		
		if(tokenizer.ttype == StreamTokenizer.TT_NUMBER) {
			return new Operand(SymbolType.NUMBER, tokenizer.nval);
		} else if(tokenizer.ttype == StreamTokenizer.TT_WORD) {
			if(SymbolUtils.isFunction(tokenizer.sval)) {
				return new Root();
			} else if(SymbolUtils.isConstant(tokenizer.sval)) {
				return newConstant(tokenizer.sval);
			}
		} else {
			String token = String.valueOf((char)tokenizer.ttype);
			
			if(SymbolUtils.isOperator(token)) {
				return newOperator(token);
			} else {
				return new Symbol(token);
			}
		}
		
		return new Symbol();
	}
}