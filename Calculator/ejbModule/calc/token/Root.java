package calc.token;

import java.util.function.BinaryOperator;

import calc.constant.Constants;

public class Root extends Operator implements BinaryOperator<Double> {
	public Root() {
		super(Constants.ROOT,20);
	}
		
	@Override
	public Double apply(Double t, Double u) {
		
		if(Double.isInfinite(t) || Double.isNaN(t)) {
			return t;
		}
		
		if(u == 1d) {
			return t;
		} else if(u == 2d) {
			return Math.sqrt(t);
		} 
		else if(u == 3d) {
			return Math.cbrt(t);
		} else if(u == 0) {
			if(t == 0) {
				return Double.POSITIVE_INFINITY;
			} else {
				return 1d;
			}
		} else {
			return Math.pow(t, 1/u);
		}
	}
}
