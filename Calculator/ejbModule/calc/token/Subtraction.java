package calc.token;

import java.util.function.BinaryOperator;

import calc.constant.Constants;

public class Subtraction extends Operator implements BinaryOperator<Double> {
	public Subtraction() {
		super(Constants.MINUS,1);
	}
		
	@Override
	public Double apply(Double t, Double u) {
		return t - u;
	}
}
