package calc.token;

import calc.constant.SymbolType;

public class Operand extends Symbol {
	private double value;
	
	public Operand() {
		super();
	}
	
	public Operand(String token) {
		super(token);
	}
	
	public Operand(SymbolType type, double value) {
		super(type);
		setValue(value);
	}
	
	public Operand(String symbolStr, double value) {
		super(symbolStr);
		setValue(value);
	}

	public double getValue() {
		if(isNumber()) {
			return value;
		} else if(isConstant()) {
			return getConstantValue(getSymbolStr());
		} else {
			return Double.NaN;
		}		
	}

	private double getConstantValue(String symbolStr) {
		switch(symbolStr) {
		case "pi":
			return Math.PI;
		case "e":
			return Math.E;
		default:
			return Double.NaN;
		}
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		if(getType() == SymbolType.NUMBER) {
			return String.valueOf(getValue());
		} else {
			return super.toString();
		}
	}
}
