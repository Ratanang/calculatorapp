package calc.token;

import calc.constant.SymbolType;

public class Symbol {
	private SymbolType type;
	private String symbolStr;

	public SymbolType getType() {
		return type;
	}

	public void setType(SymbolType type) {
		this.type = type;
	}
	
	public Symbol() {
		setType(SymbolType.UNKNOWN);
	}
	
	public Symbol(SymbolType type) {
		setType(type);
	}
	
	public Symbol(String value) {
		setSymbolStr(value);
		setType(SymbolType.of(value));
	}
	
	public Symbol(SymbolType type, String value) {
		setSymbolStr(value);
		setType(type);
	}
	
	@Override
	public String toString() {
		if(getSymbolStr() != null && !getSymbolStr().trim().isEmpty()) {
			return getSymbolStr();
		} else {
			return type.toString();
		}
	}

	public String getSymbolStr() {
		return symbolStr;
	}

	public void setSymbolStr(String symbolStr) {
		this.symbolStr = symbolStr;
	}
	
	public boolean isOperator() {
		return getType() == SymbolType.OPERATOR;
	}
	
	public boolean isNumber() {
		return getType() == SymbolType.NUMBER;
	}
	
	public boolean isFunction() {
		return getType() == SymbolType.FUNCTION;
	}
	
	public boolean isConstant() {
		return getType() == SymbolType.CONSTANT;
	}
	
	public boolean isCloseBracket() {
		return getType() == SymbolType.CLOSE_BRACKET;
	}
	
	public boolean isOpenBracket() {
		return getType() == SymbolType.OPEN_BRACKET;
	}
	
	public boolean isComma() {
		return getType() == SymbolType.COMMA;
	}
	
	public boolean isBracket() {
		return isOpenBracket() || isCloseBracket();
	}
	
	public boolean isOperand() {
		return isNumber() || isConstant();
	}
	
	public boolean isUnknown() {
		return getType() == SymbolType.UNKNOWN;
	}
	
	
}