package calc.token;

import java.util.function.BinaryOperator;

import calc.constant.Constants;

public class Multiplication extends Operator implements BinaryOperator<Double> {
	public Multiplication() {
		super(Constants.TIMES,10);
	}
		
	@Override
	public Double apply(Double t, Double u) {
		return t * u;
	}
}
