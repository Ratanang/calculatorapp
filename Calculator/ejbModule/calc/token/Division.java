package calc.token;

import java.util.function.BinaryOperator;

import calc.constant.Constants;

public class Division extends Operator implements BinaryOperator<Double> {
	public Division() {
		super(Constants.DIVIDE,10);
	}
		
	@Override
	public Double apply(Double t, Double u) {
		return t / u;
	}
}