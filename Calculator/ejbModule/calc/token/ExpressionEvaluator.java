package calc.token;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import calc.constant.Constants;
import calc.constant.CalculationType;
import calc.constant.SymbolType;
import calc.factory.SymbolFactory;

public class ExpressionEvaluator {
	private String valueStr;
	private List<Symbol> symbols;
	private CalculationType exprType;
	
	public ExpressionEvaluator(String expressionStr) {
		valueStr = expressionStr;
		initialize();
	}
	
	private void initialize() {
		symbols = tokenise(valueStr);
		handleNegation();
		setExprType(getType());
	}
	
	public Double evaluate() {
		toPostfix();
		
		return calculate(); 
	}
	
	private CalculationType getType() {
		if(symbols.size() > 3) {
			return CalculationType.ADVANCED;
		} else {
			for(Symbol symbol : symbols) {
				if(!(symbol.isNumber() || symbol.isOperator())) {
					return CalculationType.ADVANCED;
				} else if(symbol.isOperator()) {
					switch (symbol.toString()) {
					case Constants.DIVIDE:
					case Constants.MINUS:
					case Constants.PLUS:
					case Constants.TIMES:
						break;
					default:
						return CalculationType.ADVANCED;
					}
				}
			} 
		}
		
		return CalculationType.BASIC;
	}

	public List<Symbol> getSymbols() {
		return symbols;
	}

	public void setSymbols(List<Symbol> symbols) {
		this.symbols = symbols;
	}

	private List<Symbol> tokenise(String exp) {
		try {
			StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(exp));
			
			//treat the following character as ordinary characters since they have a special meaning by default: -,/,',\,"
			tokenizer.ordinaryChar('-');
			tokenizer.ordinaryChar('/');
			tokenizer.ordinaryChar('\'');
			tokenizer.ordinaryChar('\\');
			tokenizer.ordinaryChar('"');
			tokenizer.lowerCaseMode(true);//convert words to lower-case
			
			List<Symbol> symbols = new ArrayList<Symbol>();
			
			while(tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
				symbols.add(SymbolFactory.getInstance(tokenizer));
			}
			
			return symbols;
		} catch (IOException e) {
			return new ArrayList<Symbol>();
		}
	}
	
	private void handleNegation() {		
		int count = 0;
		List<Symbol> result = new ArrayList<Symbol>();
		
		for (int i = 0; i < symbols.size(); i++) {
			if ((i + 1 < symbols.size()) && Constants.MINUS.equals(symbols.get(i).toString())) {
				if (isNegation(i)) {
					if (symbols.get(i + 1).isNumber()) {
						double dValue = -1 * ((Operand) symbols.get(i + 1)).getValue();
						result.add(SymbolFactory.getInstance(String.valueOf(dValue)));
						i++;
					} else if (symbols.get(i + 1).isOpenBracket() || symbols.get(i + 1).isFunction()) {
						result.add(SymbolFactory.getInstance(Constants.OPEN_BRACKET));
						result.add(SymbolFactory.getInstance(Constants.ZERO));

						if (symbols.get(i + 1).isFunction()) {
							result.add(symbols.get(i + 1));
							i += 2;
						} else {
							i++;
						}

						count = 1;

						while (count > 0 && i < symbols.size()) {
							if (symbols.get(i).isOpenBracket()) {
								count++;
							} else if (symbols.get(i).isCloseBracket()) {
								count--;
							}
							result.add(symbols.get(i));
							i++;
						}

						result.add(SymbolFactory.getInstance(Constants.CLOSE_BRACKET));
					} else {
						result.add(SymbolFactory.getInstance(Constants.OPEN_BRACKET));
						result.add(SymbolFactory.getInstance(Constants.ZERO));
						result.add(symbols.get(i));
						result.add(symbols.get(i + 1));
						result.add(SymbolFactory.getInstance(Constants.CLOSE_BRACKET));
						i++;
					}
				} else {
					result.add(symbols.get(i));
				}
			} else {
				result.add(symbols.get(i));
			}
		}
		
		symbols = result;
	}
	
	private boolean isNegation(int currIndex) {
		
		boolean isNeg = false;
		
		//if "-" symbol is first in the list, occurs directly after an operator or after an opening brace
		if(currIndex == 0 || symbols.get(currIndex - 1).isOperator() || symbols.get(currIndex - 1).isOpenBracket()) {
			//if the next symbol after is not any of the following: any operator, "(" or any unkown symbol 
			if(currIndex + 1 < symbols.size() && !symbols.get(currIndex + 1).isCloseBracket() && !symbols.get(currIndex + 1).isOperator() && !symbols.get(currIndex + 1).isUnknown()) {
				isNeg = true;
			}
		}

		return isNeg;
	}
	
	private final void toPostfix(){
		Operator currOperator = null;
		Operator peekOperator = null;
		
		List<Symbol> result = new ArrayList<Symbol>();
		Stack<Symbol> operatorStack = new Stack<Symbol>();
		
		for(Symbol symbol : symbols) {
			if(symbol.isConstant() || symbol.isNumber()) {
				result.add(symbol);
			} else if(symbol.isOpenBracket() || symbol.isFunction()) {
				operatorStack.push(symbol);
			} else if(symbol.isCloseBracket()) {
				while(!operatorStack.isEmpty()) {
					if(operatorStack.peek().isOpenBracket()) {
						operatorStack.pop();
						
						if(!operatorStack.isEmpty() && operatorStack.peek().isFunction()) {
							result.add(operatorStack.pop());
						}
						break;
					} else {
						result.add(operatorStack.pop());
					}
				}
			} else if(symbol.isOperator()) {
				currOperator = (Operator)symbol;
				
				while(!operatorStack.isEmpty() && operatorStack.peek().isOperator()) {
					peekOperator = (Operator)operatorStack.peek();
					
					if(peekOperator.getPrecedence() >= currOperator.getPrecedence()) {
						result.add(operatorStack.pop());
					} else {
						break;
					}
				}
				operatorStack.push(currOperator);
			} if(symbol.isComma()) {
				while(!operatorStack.isEmpty() && !operatorStack.peek().isOpenBracket()) {
						result.add(operatorStack.pop());
				}
			}
			else if(symbol.isUnknown()) {
				throw new InvalidParameterException("Unknow symbol: " + symbol.toString());
			}
		}
		
		while(!operatorStack.isEmpty()) {
			result.add(operatorStack.pop());
		}
		
		symbols = result;
	}

	private Double calculate(){
		double result = 0;
		Operator currOperator = null;
		Double leftOper = null;
		Double rightOper = null;
		Stack<Operand> operandStack = new Stack<Operand>();
		
		for(Symbol symbol : symbols) {
			if(symbol.isConstant() || symbol.isNumber()) {
				operandStack.push((Operand)symbol);
			} else if(symbol.isOperator() || symbol.isFunction()) {
				if(operandStack.size() > 1) {
					currOperator = (Operator)symbol;
					rightOper = operandStack.pop().getValue();
					leftOper = operandStack.pop().getValue();
					Double calcValue = currOperator.apply(leftOper, rightOper);
					operandStack.push(new Operand(SymbolType.NUMBER, calcValue));
				} else {
					throw new InvalidParameterException();
				}
			} else {
				throw new InvalidParameterException();
			}
		}
		
		if(operandStack.size() == 1) {
			result = operandStack.pop().getValue();
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		ExpressionEvaluator exp1 = new ExpressionEvaluator("(5+3-2*(9-3))^4");
		ExpressionEvaluator exp2 = new ExpressionEvaluator("pi*e^3");
		ExpressionEvaluator exp3 = new ExpressionEvaluator("e*3^pi");
		System.out.println(exp1 + " = " + exp1.evaluate());
		System.out.println(exp2 + " = " + exp2.evaluate());
		System.out.println(exp3 + " = " + exp3.evaluate());
	}
	
	@Override
	public String toString() {
		return valueStr;
	}

	public CalculationType getExprType() {
		return exprType;
	}

	public void setExprType(CalculationType exprType) {
		this.exprType = exprType;
	}
}
