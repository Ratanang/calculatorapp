package calc.token;

import java.util.function.BinaryOperator;

import calc.constant.Constants;

public class Addition extends Operator implements BinaryOperator<Double> {
	public Addition() {
		super(Constants.PLUS,1);
	}
		
	@Override
	public Double apply(Double t, Double u) {
		return t + u;
	}
}
