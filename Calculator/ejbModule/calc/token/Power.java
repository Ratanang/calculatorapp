package calc.token;

import java.util.function.BinaryOperator;

import calc.constant.Constants;

public class Power extends Operator implements BinaryOperator<Double> {
	public Power() {
		super(Constants.POWER,15);
	}
		
	@Override
	public Double apply(Double t, Double u) {
		return Math.pow(t, u);
	}
}
