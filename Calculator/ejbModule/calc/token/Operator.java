package calc.token;

import java.util.function.BinaryOperator;

import calc.constant.SymbolType;

public abstract class Operator extends Symbol implements BinaryOperator<Double>{
	private int precedence;
	
	public Operator(int precendence) {
		this(SymbolType.OPERATOR, precendence);
	}
	
	public Operator(String symbol, int precendence) {
		super(symbol);
		setPrecedence(precendence);
	}
	
	public Operator(SymbolType type, int precendence) {
		super(type);
		setPrecedence(precendence);
	}

	public int getPrecedence() {
		return precedence;
	}

	public void setPrecedence(int precedence) {
		this.precedence = precedence;
	}
}