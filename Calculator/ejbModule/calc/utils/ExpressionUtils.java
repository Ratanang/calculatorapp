package calc.utils;

import java.util.List;
import java.util.stream.Collectors;

import calc.constant.Constants;
import calc.token.Symbol;

public class ExpressionUtils {
	public static boolean isOperator(String token) {
		return Constants.OPERATORS.contains(token);
	}
	
	public static boolean isFunction(String token) {
		return Constants.FUNCTIONS.contains(token);
	}
	
	public static boolean isConstant(String token) {
		return Constants.CONSTANTS.contains(token);
	}
	
	public static boolean isBracket(String token) {
		return isOpenBracket(token) || isCloseBracket(token);
	}
	
	public static boolean isOpenBracket(String token) {
		return Constants.OPEN_BRACKET.equals(token);
	}
	
	public static boolean isCloseBracket(String token) {
		return Constants.CLOSE_BRACKET.equals(token);
	}
	
	public static boolean isComma(String token) {
		return Constants.COMMA.equals(token);
	}
	
	public static boolean isNumber(String token) {
		try {
			Double.parseDouble(token);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isTerm(String token) {
		return isNumber(token) || isConstant(token);
	}
	
	public static double getNumber(String token) {
		return Double.parseDouble(token);
	}
	
	public static String toString(List<Symbol> symbols) {
		if(symbols == null || symbols.isEmpty()) {
			return Constants.EMPTY_STRING;
		} else {
			return symbols.stream().map(Symbol::toString).collect(Collectors.joining());
		}
	}
}
