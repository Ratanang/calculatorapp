package calc.constant;

import java.util.Arrays;
import java.util.List;

public class Constants {
	public static final String OPEN_BRACKET = "(";
	public static final String CLOSE_BRACKET = ")";
	public static final String COMMA = ",";
	public static final String MINUS = "-";
	public static final String ZERO = "0";
	public static final String PLUS = "+";
	public static final String TIMES = "*";
	public static final String DIVIDE = "/";
	public static final String POWER = "^";
	public static final String ROOT = "root";
	public static final String E = "e";
	public static final String PI = "pi";
	public static final List<String> OPERATORS = Arrays.asList(PLUS,MINUS,POWER,DIVIDE,TIMES);
	public static final List<String> FUNCTIONS = Arrays.asList(ROOT);
	public static final List<String> CONSTANTS = Arrays.asList(PI,E);
	public static final String EMPTY_STRING = "";
}