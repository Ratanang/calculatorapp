package calc.constant;

import calc.utils.SymbolUtils;

public enum SymbolType {
	FUNCTION, CONSTANT, OPERATOR, NUMBER, OPEN_BRACKET, CLOSE_BRACKET, UNKNOWN, COMMA;
	
	public static SymbolType of(String token) {
		
		if(token == null || token.isEmpty()) {
			return UNKNOWN;
		} else if(Constants.CONSTANTS.contains(token)) {
			return CONSTANT;
		} else if(Constants.FUNCTIONS.contains(token)) {
			return FUNCTION;
		} else if(Constants.OPERATORS.contains(token)) {
			return OPERATOR;
		} else if(SymbolUtils.isNumber(token)) {
			return NUMBER;
		} else if(Constants.CLOSE_BRACKET.equals(token)){
			return CLOSE_BRACKET;
		} else if(Constants.OPEN_BRACKET.equals(token)) {
			return OPEN_BRACKET;
		} else if(Constants.COMMA.equals(token)) {
			return COMMA;
		}else {		
			return UNKNOWN;
		}
	}
}
