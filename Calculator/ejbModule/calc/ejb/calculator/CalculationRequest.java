package calc.ejb.calculator;

import java.io.Serializable;

public class CalculationRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3525295043553490070L;
	private String expression;
	private String userName;
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
