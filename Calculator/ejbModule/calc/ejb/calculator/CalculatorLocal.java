package calc.ejb.calculator;

import javax.ejb.Local;

@Local
public interface CalculatorLocal {
	public double calculate(CalculationRequest request, boolean isAdvanced);
}