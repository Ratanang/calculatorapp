package calc.ejb.calculator.impl;

import java.sql.Timestamp;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import calc.constant.CalculationType;
import calc.ejb.calculator.CalculationRequest;
import calc.ejb.calculator.CalculatorLocal;
import calc.entity.Calculation;
import calc.entity.User;
import calc.entity.dao.CalculationDAO;
import calc.entity.dao.UserDAO;
import calc.token.ExpressionEvaluator;

/**
 * Session Bean implementation class CalculatorBean
 */
@Stateless
public class CalculatorBean implements CalculatorLocal {
	@PersistenceContext(unitName = "calculator",type=PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;
	
    public CalculatorBean() {
    }

	@Override
	public double calculate(CalculationRequest request, boolean isAdvanced) {
		Calculation expression = buildExpression(request);
		
		if (expression != null) {
			evaluateExpression(expression, isAdvanced);
			saveExpression(expression);

			return expression.getResult();
		} else {
			return Double.NaN;
		}
	}

	private Calculation buildExpression(CalculationRequest request) {
		if(request != null && request.getUserName() != null && request.getExpression() != null) {
			UserDAO dao = new UserDAO();
			
			User user = dao.getByUserName(request.getUserName());
			
			if(user != null) {
				Calculation expression = new Calculation();
				expression.setUser(user);
				expression.setExpression(request.getExpression());
				
				return expression;
			}
		}
		
		return null;
	}

	private void saveExpression(Calculation expression) {		
		CalculationDAO dao = new CalculationDAO();
		dao.save(expression);
	}

	private Double evaluateExpression(Calculation expression, boolean isAdvanced) {
		Double result = Double.NaN;
		
		try {
			ExpressionEvaluator evaluator = new ExpressionEvaluator(expression.getExpression());
			result = evaluator.evaluate();
			expression.setResult(result);
			expression.setExprType(isAdvanced ? CalculationType.ADVANCED.toString() : CalculationType.BASIC.toString());
			expression.setStatus("SUCCESS");
			
			return result;
		} catch (Exception ex) {
			expression.setStatus("FAILURE");
		} finally {
			expression.setLogTimestamp(getCurrentTimeStamp());
		}
		
		return result;
	}

	private Timestamp getCurrentTimeStamp() {
		return new Timestamp(System.currentTimeMillis());
	}

}
