package calc.ejb.user;

public class AddUserRequest extends UserResponse {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9033529254066103919L;
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
