package calc.ejb.user.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import calc.ejb.user.AddUserRequest;
import calc.ejb.user.AuthRequest;
import calc.ejb.user.AuthResponse;
import calc.ejb.user.CalculationResponse;
import calc.ejb.user.UserLocal;
import calc.ejb.user.UserRequest;
import calc.ejb.user.UserResponse;
import calc.entity.Calculation;
import calc.entity.User;
import calc.entity.dao.CalculationDAO;
import calc.entity.dao.UserDAO;

/**
 * Session Bean implementation class UserBean2
 */
@Stateless
public class UserBean implements UserLocal {

    public UserBean() {
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public UserResponse getUser(UserRequest request) {
		
		UserResponse response = null;
		
		if(request != null) {
			response = getUser(request.getUserId());
		}
		
		return response;
	}

	private UserResponse getUser(int userId) {
		UserResponse response = new UserResponse();
		UserDAO dao = new UserDAO();
		User user = dao.getById(userId);
		
		if(user != null) {
			response.setId(userId);
			response.setAdmin(user.getIsAdmin());
			response.setFirstName(user.getFirstName());
			response.setLastName(user.getLastName());
			response.setUserName(user.getUserName());
		}
		return response;
	}

	@Override
	public AuthResponse authenticate(AuthRequest request) {
		AuthResponse response = null;
		
		if(request != null) {
			String userName = request.getUserName();
			String password = request.getPassword();
			
			if(userName == null || password == null || userName.trim().isEmpty() || password.trim().isEmpty()) {
				response = new AuthResponse();
				response.setAuthenticated(false);
			} else {
				UserDAO dao = new UserDAO();
				
				User user = dao.getByUserName(userName);
				
				if(user != null && userName.equals(user.getUserName()) && password.equals(user.getPassword())) {
					response = new AuthResponse();
					response.setUser(getUser(user.getId()));
					response.setAuthenticated(true);
				} else {
					response = new AuthResponse();
					response.setAuthenticated(false);
				}
			}
		}
		
		return response;
	}

	@Override
	public List<CalculationResponse> getExpressions(UserRequest request) {
		List<CalculationResponse> response = new ArrayList<CalculationResponse>();
		
		if(request != null) {
			int userId = request.getUserId();
			UserDAO dao = new UserDAO();
			User user = dao.getById(userId);
			
			if(user != null) {
				response = getExpressions(user);
			}
		}
		
		return response;
	}
	
	public List<CalculationResponse> getExpressionsForAdmin(UserRequest request) {
		List<CalculationResponse> response = new ArrayList<CalculationResponse>();
		
		if(request != null) {
			int userId = request.getUserId();
			UserDAO dao = new UserDAO();
			User user = dao.getById(userId);
			
			if(user != null && user.getIsAdmin()) {
					for(User obj : dao.getAll()) {
						response.addAll(getExpressions(obj));
					}
			}
		}
		
		return response;
	}

	private List<CalculationResponse> getExpressions(User user) {
		List<CalculationResponse> response = new ArrayList<CalculationResponse>();
		
		for(Calculation expression : user.getExpressions()) {
			CalculationResponse expr = toCalculationResponse(expression);
			
			response.add(expr);
		}
		
		return response;
	}

	private CalculationResponse toCalculationResponse(Calculation expression) {
		CalculationResponse expr = new CalculationResponse();
		expr.setUserName(expression.getUser().getUserName());
		expr.setExprType(expression.getExprType());
		expr.setLogTimestamp(expression.getLogTimestamp());
		expr.setResult(expression.getResult());
		expr.setExpression(expression.getExpression());
		return expr;
	}

	@Override
	public UserResponse addUser(AddUserRequest request) {
		
		User user = new User();
		user.setFirstName(request.getFirstName());
		user.setPassword(request.getPassword());
		user.setIsAdmin(request.isAdmin());
		user.setLastName(request.getLastName());
		user.setUserName(request.getUserName());
		
		UserDAO dao = new UserDAO();
		if(dao.save(user)) {
			user = dao.getByUserName(request.getUserName());
			return getUser(user.getId());
		} else {
			return null;
		}
	}

	@Override
	public List<CalculationResponse> findByDate(Date date) {
		List<CalculationResponse> response = null;
		
		CalculationDAO dao = new CalculationDAO();
		List<Calculation> expressions = dao.getByDate(date);
		
		if(expressions != null) {
			response = new ArrayList<CalculationResponse>();
			for(Calculation exp : expressions) {
				response.add(toCalculationResponse(exp));
			}
		}
		
		return response;
	}

	@Override
	public List<CalculationResponse> findByUserName(String userName) {
		return findByUserNameAndDateRange(userName, null, null);
	}

	@Override
	public List<CalculationResponse> findByDateRange(Date from, Date to) {
		return findByUserNameAndDateRange(null, from, to);
	}

	@Override
	public List<CalculationResponse> findByUserNameAndDateRange(String userName, Date from, Date to) {
		List<CalculationResponse> response = null;
		
		CalculationDAO dao = new CalculationDAO();
		List<Calculation> calculation = dao.getByUserNameAndDateRange(userName, from, to);
		
		if(calculation != null) {
			response = new ArrayList<CalculationResponse>();
			for(Calculation exp : calculation) {
				response.add(toCalculationResponse(exp));
			}
		}
		
		return response;
	}

}
