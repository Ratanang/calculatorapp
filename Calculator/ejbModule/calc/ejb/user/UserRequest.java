package calc.ejb.user;

import java.io.Serializable;

public class UserRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -884638012125938503L;
	private int userId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}