package calc.ejb.user;

import java.io.Serializable;

public class AuthResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3814295720679981459L;
	private boolean authenticated;
	private UserResponse user;
	
	public boolean isAuthenticated() {
		return authenticated;
	}
	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}
	public UserResponse getUser() {
		return user;
	}
	public void setUser(UserResponse user) {
		this.user = user;
	}
}
