package calc.ejb.user;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface UserLocal {
	public UserResponse addUser(AddUserRequest request);
	public UserResponse getUser(UserRequest request);
	public AuthResponse authenticate(AuthRequest request);
	public List<CalculationResponse> getExpressions(UserRequest request);
	public List<CalculationResponse> getExpressionsForAdmin(UserRequest request);
	public List<CalculationResponse> findByDate(Date date);
	public List<CalculationResponse> findByUserName(String selectedValue);
	public List<CalculationResponse> findByDateRange(Date from, Date to);
	public List<CalculationResponse> findByUserNameAndDateRange(String userName, Date from, Date to);
}
