package calc.ejb.user;

import java.io.Serializable;

public class UserResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6514150625182203995L;
	private String firstName;
	private boolean admin;
	private String lastName;
	private String userName;
	private Integer id;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
