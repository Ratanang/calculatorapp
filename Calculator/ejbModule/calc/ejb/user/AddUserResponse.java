package calc.ejb.user;

public class AddUserResponse extends UserResponse {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1986945623762552535L;
	private int userId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}
