package calc.ejb.user;

import java.io.Serializable;
import java.util.Date;

public class CalculationResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 435543563504015309L;
	private String exprType;
	private String expression;
	private Date logTimestamp;
	private double result;
	private String userName;
	
	public String getExprType() {
		return exprType;
	}
	public void setExprType(String exprType) {
		this.exprType = exprType;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public Date getLogTimestamp() {
		return logTimestamp;
	}
	public void setLogTimestamp(Date logTimestamp) {
		this.logTimestamp = logTimestamp;
	}
	public double getResult() {
		return result;
	}
	public void setResult(double result) {
		this.result = result;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}