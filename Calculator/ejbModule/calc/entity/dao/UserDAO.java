package calc.entity.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import calc.entity.User;

public class UserDAO {
	private EntityManager entityManager;
	
	public boolean save(User user) {
		try {
			getEntityManager();
			entityManager.persist(user);
			entityManager.flush();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void getEntityManager() {
		if(entityManager == null) {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("calculator");
			entityManager = factory.createEntityManager();
		}
	}
	
	public User getById(int id) {
		User user = null;
		
		try {
			getEntityManager();
			user = entityManager.find(User.class,id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	public User getByUserName(String userName) {
		User user = null;
		
		try {
			getEntityManager();
			TypedQuery<User> query = entityManager.createQuery("Select u FROM Tbuser u WHERE u.userName = ?1", User.class);
			query.setParameter(1, userName);
			user = query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	public List<User> getAll() {
		List<User> users = null;
		
		try {
			getEntityManager();
			TypedQuery<User> query = entityManager.createQuery("SELECT t FROM Tbuser t", User.class);
			users = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return users;
	}
}