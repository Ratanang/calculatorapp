package calc.entity.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import calc.entity.Calculation;
import calc.entity.User;

public class CalculationDAO{
	@PersistenceContext(unitName = "calculator")
	private EntityManager entityManager;
	
	public boolean save(Calculation expr) {
		try {
			getEntityManager();
			entityManager.persist(expr);
			entityManager.flush();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private void getEntityManager() {
		if(entityManager == null) {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("calculator");
			entityManager = factory.createEntityManager();
		}
	}
	
	public List<Calculation> getAll() {
		List<Calculation> expression = null;
		
		try {
			getEntityManager();
			TypedQuery<Calculation> query = entityManager.createQuery(Calculation.FIND_ALL, Calculation.class);
			expression = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return expression;
	}
	
	public List<Calculation> getByUserName(String userName) {
		List<Calculation> expression = null;
		
		try {
			getEntityManager();
			User user = new UserDAO().getByUserName(userName);

			if(user != null) {
				expression = new ArrayList<Calculation>(user.getExpressions());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return expression;
	}
	
	public List<Calculation> getByDateRange(Date from, Date to) {
		TypedQuery<Calculation> query = null;
		List<Calculation> expression = null;

		try {
			getEntityManager();
			if (from != null && to != null) {
				query = entityManager.createQuery("Select e FROM Tbcalculation e WHERE e.logTimestamp BETWEEN ?1 AND ?2",
						Calculation.class);
				query.setParameter(1, getStartOfDay(from));
				query.setParameter(2, getEndOfDay(to));
			} else if (from != null) {
				query = entityManager.createQuery("Select e FROM Tbcalculation e WHERE e.logTimestamp >= ?1 ",
						Calculation.class);
				query.setParameter(1, getStartOfDay(from));
			} else if (to != null) {
				query = entityManager.createQuery("Select e FROM Tbcalculation e WHERE e.logTimestamp <= ?1",
						Calculation.class);
				query.setParameter(1, getEndOfDay(to));
			} else {
				return expression;
			}
			expression = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return expression;
	}
	
	public List<Calculation> getByUserNameAndDateRange(String userName, Date from, Date to) {
		
		List<Calculation> calculation = new ArrayList<Calculation>();
		if(userName == null || userName.isEmpty()) {
			calculation = getByDateRange(from, to);
		} else {
			calculation = getByUserName(userName);
			
			if(calculation != null) {
				if(from != null) {
					calculation = calculation.stream().filter(x -> !x.getLogTimestamp().before(getStartOfDay(from))).collect(Collectors.toList());
				}
				if(to != null) {
					calculation = calculation.stream().filter(x -> !x.getLogTimestamp().after(getEndOfDay(to))).collect(Collectors.toList());
				}
			}
		}

		return calculation;
	}
	
	public List<Calculation> getByDate(Date date) {
		return getByDateRange(date, date);
	}
	
	private Date getStartOfDay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR,00);
		cal.set(Calendar.MINUTE,00);
		cal.set(Calendar.SECOND,00);
		cal.set(Calendar.MILLISECOND, 00);
		
		return cal.getTime();
	}
	
	private Date getEndOfDay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR,23);
		cal.set(Calendar.MINUTE,59);
		cal.set(Calendar.SECOND,59);
		cal.set(Calendar.MILLISECOND, 999);
		
		return cal.getTime();
	}
}