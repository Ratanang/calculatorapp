package calc.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the tbuser database table.
 * 
 */
@Entity(name="Tbuser")
@NamedQuery(name=User.FIND_ALL, query="SELECT t FROM Tbuser t")
public class User implements Serializable {
	public static final String FIND_ALL = "User.findAll";

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="first_name")
	private String firstName;

	@Column(name="is_admin")
	private Boolean isAdmin;

	@Column(name="last_name")
	private String lastName;

	private String password;

	@Column(name="user_name")
	private String userName;

	//bi-directional many-to-one association to Calculation
	@OneToMany(mappedBy="user")
	private Set<Calculation> expressions;

	public User() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getIsAdmin() {
		return this.isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Set<Calculation> getExpressions() {
		return this.expressions;
	}

	public void setTbexpressions(Set<Calculation> expression) {
		this.expressions = expression;
	}

	public Calculation addTbexpression(Calculation expression) {
		getExpressions().add(expression);
		expression.setUser(this);

		return expression;
	}

	public Calculation removeExpression(Calculation expression) {
		getExpressions().remove(expression);
		expression.setUser(null);

		return expression;
	}

}