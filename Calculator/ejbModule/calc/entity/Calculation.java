package calc.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the tbexpression database table.
 * 
 */
@Entity(name="Tbcalculation")
@NamedQuery(name=Calculation.FIND_ALL, query="SELECT t FROM Tbcalculation t")
public class Calculation implements Serializable {
	public static final String FIND_ALL = "Calculation.findAll";

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="expr_type")
	private String exprType;

	private String expression;

	@Column(name="log_timestamp")
	private Timestamp logTimestamp;

	private double result;

	private String status;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	public Calculation() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExprType() {
		return this.exprType;
	}

	public void setExprType(String exprType) {
		this.exprType = exprType;
	}

	public String getExpression() {
		return this.expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public Timestamp getLogTimestamp() {
		return this.logTimestamp;
	}

	public void setLogTimestamp(Timestamp logTimestamp) {
		this.logTimestamp = logTimestamp;
	}

	public double getResult() {
		return this.result;
	}

	public void setResult(double result) {
		this.result = result;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}